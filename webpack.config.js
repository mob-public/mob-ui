const path = require('path')
const nodeExternals = require('webpack-node-externals')
const { loader } = require('styled-jsx/webpack')

module.exports = {
  entry: {
    Button: './src/components/Button/index.jsx',
    ButtonCustom: './src/components/ButtonCustom/index.jsx',
    Input: './src/components/Input/index.jsx',
    Overlay: './src/components/Overlay/index.jsx',
    Dropdown: './src/components/Dropdown/index.jsx',
    Popup: './src/components/Popup/index.jsx',
    Popover: './src/components/Popover/index.jsx',
    List: './src/components/List/List/index.jsx',
    ListItem: './src/components/List/Item/index.jsx',
    ListItemExtended: './src/components/List/ItemExtended/index.jsx',
    Loader: './src/components/Loader/index.jsx',
    LoaderLinear: './src/components/LoaderLinear/index.jsx',
    TagList: './src/components/TagList/index.jsx',
    Photostatus: './src/components/Photostatus/index.jsx',
    TabsMenu: './src/components/TabsMenu/List/index.jsx',
    TabsMenuItem: './src/components/TabsMenu/Item/index.jsx',
    LinedTitle: './src/components/LinedTitle/index.jsx',
    Paper: './src/components/Paper/index.jsx',
    Checkbox: './src/components/Checkbox/index.jsx',
    Textarea: './src/components/Textarea/index.jsx',
  },
  output: {
    path: path.resolve(__dirname, './'),
    filename: '[name].js',
    library: '',
    libraryTarget: 'commonjs',
  },
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.(js|jsx|css)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
        },
        resolve: {
          extensions: ['*', '.js', '.jsx'],
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader,
            options: {
              type: 'scoped',
            },
          },
        ],
      },
    ],
  },
}
