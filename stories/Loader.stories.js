import React from 'react'
import { text, number, withKnobs } from '@storybook/addon-knobs'
import { Loader } from '../src/components/Loader'
import { Block } from './Block'

export default {
  title: 'Loader',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <Loader
      color={text('Color', '#FF0000')}
      size={number('Size', 50)}
      height={number('Height', 200)}
      borderWidth={number('Border width', 5)}
      duration={text('Animation duration', '1.2s')}
    />
  </Block>
)
