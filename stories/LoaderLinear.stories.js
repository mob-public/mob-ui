import React from 'react'
import { text, number, withKnobs } from '@storybook/addon-knobs'
import { LoaderLinear } from '../src/components/LoaderLinear'
import { Block } from './Block'

export default {
  title: 'LoaderLinear',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <LoaderLinear
      height={number('Height', 4)}
      backgroundColor={text('Background color', '#B3E5FC')}
      primaryColor={text('Primary color', '#03A9F4')}
      secondaryColor={text('Secondary color', '#4FC3F7')}
      duration={text('Animation duration', '1.5s')}
    />
  </Block>
)
