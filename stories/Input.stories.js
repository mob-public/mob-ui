import React from 'react'
import { withKnobs, text, select } from '@storybook/addon-knobs'
import { Input, themes, sizes, shapes } from '../src/components/Input'
import { Button } from '../src/components/Button'
import { Block } from './Block'

export default {
  title: 'Input',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <Input
      style={{ width: 300 }}
      theme={select('Theme', themes, 'bright')}
      placeholder={text('Placeholder', 'Placeholder')}
      value={text('Value', '')}
      shape={select('Shape', shapes, 'box')}
      sizes={select('Size', sizes, 'middle')}
    />
  </Block>
)

export const Docs = () => (
  <>
    <Block title="Themes">
      <div style={{ display: 'grid', gridGap: 20, gridTemplateColumns: '200px 200px 200px' }}>
        <Input theme="bright" placeholder="Theme Bright" />
        <Input theme="dark" placeholder="Theme Dark" />
        <Input theme="gray" placeholder="Theme Gray" />
      </div>
    </Block>

    <Block title="Sizes">
      <div style={{ display: 'grid', gridGap: 20, gridTemplateColumns: '200px 200px 200px' }}>
        <Input theme="bright" size="small" placeholder="Size small" />
        <Input theme="bright" size="middle" placeholder="Size middle" />
        <Input theme="bright" size="big" placeholder="Size big" />
      </div>
    </Block>

    <Block title="Shapes">
      <div style={{ display: 'grid', gridGap: 20, gridTemplateColumns: '200px 200px 200px' }}>
        <Input theme="bright" shape="box" placeholder="Shape box" />
        <Input theme="bright" shape="round" placeholder="Shape round" />
      </div>
    </Block>

    <Block title="With Buttons">
      <div style={{ display: 'grid', gridGap: 20, gridTemplateColumns: '200px 200px 200px' }}>
        <Input theme="bright" leftElement={<Button theme="green" shape="box" icon="<" />} placeholder="Left element" />
        <Input theme="bright" rightElement={<Button theme="green" shape="box" icon=">" />} placeholder="Right element" />
      </div>
    </Block>
  </>
)
