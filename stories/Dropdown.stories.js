import React from 'react'
import { withKnobs, boolean, number, select } from '@storybook/addon-knobs'
import { Button } from '../src/components/Button'
import { Dropdown, positions, shapes } from '../src/components/Dropdown'
import { Block } from './Block'

export default {
  title: 'Dropdown',
  decorators: [withKnobs],
}

export const Playground = () => (
  <>
    <Block title="Position">
      <Dropdown
        position={select('Position', positions, 'center')}
        shape={select('Shape', shapes, 'square')}
        shadowOpacity={number('Shadow opacity', 0.05)}
        withPointer={boolean('Pointer', false)}
        content={<div style={{ width: 200, height: 100, background: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center' }} children="Children" />}
        children={(isOpen) => <Button theme="black">{isOpen ? 'Close Dropdown' : 'Open Dropdown'}</Button>}
      />
    </Block>

    <Block title="Function content">
      <Dropdown
        position={select('Position', positions, 'center')}
        shape={select('Shape', shapes, 'square')}
        shadowOpacity={number('Shadow opacity', 0.05)}
        withPointer={boolean('Pointer', false)}
        content={(onClose) => (
          <div
            onClick={onClose}
            style={{ width: 200, height: 100, background: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            children="Children"
          />
        )}
        children={(isOpen) => <Button theme="black">{isOpen ? 'Close Dropdown' : 'Open Dropdown'}</Button>}
      />
    </Block>
  </>
)
