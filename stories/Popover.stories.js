import React, { useState } from 'react'
import { number, text, withKnobs } from '@storybook/addon-knobs'
import { Button } from '../src/components/Button'
import { Popover } from '../src/components/Popover'
import { Block } from './Block'

export default {
  title: 'Popover',
  decorators: [withKnobs],
}

export const Playground = () => {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <Block title="Default">
      <Button onClick={() => setIsOpen(true)} theme="black">
        Open popover
      </Button>
      {isOpen && (
        <Popover
          onCloseRequest={() => setIsOpen(false)}
          marginTop={text('Margin top', '150px')}
        >
          <div style={{
            height: number('Height', 200),
            background: text('Background', '#FFFFFF'),
          }}
          />
        </Popover>
      )}
    </Block>
  )
}
