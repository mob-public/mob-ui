import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { Block } from './Block'
import { List } from '../src/components/List/List'
import { ListItem } from '../src/components/List/Item'
import { ListItemExtended } from '../src/components/List/ItemExtended'

export default {
  title: 'List',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="List">
    <List style={{ width: 210 }} bordered>
      <ListItemExtended item="List Item 1" iconClose="<" iconOpen=">">
        <List bordered>
          <ListItem>List Item extended 1</ListItem>
          <ListItem>List Item extended 2</ListItem>
        </List>
      </ListItemExtended>
      <ListItem>List Item 2</ListItem>
      <ListItem>List Item 3</ListItem>
      <ListItem>List Item 4</ListItem>
      <ListItem>List Item 5</ListItem>
      {null}
    </List>
  </Block>
)
