import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { Block } from './Block'
import { Textarea } from '../src/components/Textarea'

export default {
  title: 'Textarea',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <Textarea
      style={{ height: text('Height', '100px') }}
    />
  </Block>
)
