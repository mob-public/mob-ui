import React, { useState } from 'react'
import { withKnobs, text, select } from '@storybook/addon-knobs'
import { TabsMenu } from '../src/components/TabsMenu/List'
import { TabsMenuItem } from '../src/components/TabsMenu/Item'
import { Block } from './Block'

export default {
  title: 'TabsMenu',
  decorators: [withKnobs],
}

export const Playground = () => {
  const [currentHref, setCurrentHref] = useState('')

  const onClick = (e) => {
    e.preventDefault()
    setCurrentHref(e.currentTarget.getAttribute('href'))
  }

  return (
    <Block title="Playground">
      <div className="container">
        <TabsMenu
          className={select('Class name', ['', 'menu_small'], '')}
        >
          <a href="/" onClick={onClick}>
            <TabsMenuItem isActive={currentHref === '/'}>
              {text('Left label', 'Top')}
            </TabsMenuItem>
          </a>
          <a href="" onClick={onClick}>
            <TabsMenuItem isActive={currentHref === ''}>
              {text('Right label', 'New')}
            </TabsMenuItem>
          </a>
        </TabsMenu>
      </div>

      <style jsx>{`
        .container {
          padding: 40px;
          background: #888888;
        }
        
        .container :global(.menu_small) {
          width: 160px;
          height: 30px;
          border-radius: 15px;
        }
        
        @media (min-width: 768px) {
          .container :global(.menu_small) {
            width: 200px;
          }
        }
    `}</style>
    </Block>
  )
}
