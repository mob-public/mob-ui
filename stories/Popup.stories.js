import React, { useState } from 'react'
import { text, withKnobs } from '@storybook/addon-knobs'
import { Button } from '../src/components/Button'
import { Popup } from '../src/components/Popup'
import { Block } from './Block'

export default {
  title: 'Popup',
  decorators: [withKnobs],
}

export const Playground = () => {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <Block title="Default">
      <Button onClick={() => setIsOpen(true)} theme="black">
        Open popup
      </Button>
      {isOpen && (
        <Popup
          onCloseRequest={() => setIsOpen(false)}
          margin={text('Margin', '0')}
        >
          <div
            style={{
              width: 200,
              height: text('Height', '400px'),
              background: '#FFFFFF',
            }}
          />
        </Popup>
      )}
    </Block>
  )
}
