import React from 'react'
import { number, text, withKnobs } from '@storybook/addon-knobs'
import { Block } from './Block'
import { Checkbox } from '../src/components/Checkbox'

export default {
  title: 'Checkbox',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <div className="container">
      <Checkbox
        id="testId"
        label={(
          <div className="label">{text('Label', 'Label')}</div>
        )}
        size={number('Size', 25)}
        color={text('Color', '#1BB378')}
        borderRadius={number('Border radius', 5)}
      />
    </div>

    <style jsx>{`
      .container { display: flex; }
      .label { padding-left: 10px; }
  `}</style>
  </Block>
)
