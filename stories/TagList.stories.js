import React from 'react'
import PropTypes from 'prop-types'
import { withKnobs, text } from '@storybook/addon-knobs'
import { Block } from './Block'
import { TagList } from '../src/components/TagList'
import { Button } from '../src/components/Button'

export default {
  title: 'TagList',
  decorators: [withKnobs],
}

const TagListItem = ({ href, children }) => (
  <Button
    href={href}
    theme="white-gray-2"
    shape="circle"
    size={35}
    short
    block
  >
    {children}
  </Button>
)

TagListItem.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string,
}

TagListItem.defaultProps = {
  href: undefined,
}

export const Playground = () => (
  <Block title="List">
    <div className="container">
      <TagList
        showAllButton={<TagListItem>...</TagListItem>}
        rightButton={<TagListItem href="/">{text('Right Button', 'All tags')}</TagListItem>}
      >
        <TagListItem href="/">{text('Label 1', 'Tag 1')}</TagListItem>
        <TagListItem href="/">{text('Label 2', 'Tag 2')}</TagListItem>
        <TagListItem href="/">{text('Label 3', 'Long Tag 3')}</TagListItem>
        <TagListItem href="/">{text('Label 4', 'Long Tag  4')}</TagListItem>
        <TagListItem href="/">{text('Label 5', 'Long Tag  5')}</TagListItem>
        <TagListItem href="/">{text('Label 6', 'Long Tag  6')}</TagListItem>
        <TagListItem href="/">{text('Label 7', 'Very Long Tag  7')}</TagListItem>
        <TagListItem href="/">{text('Label 8', 'Tag 8')}</TagListItem>
        <TagListItem href="/">{text('Label 9', 'Long Tag 9')}</TagListItem>
        <TagListItem href="/">{text('Label 10', 'Long Tag  10')}</TagListItem>
        <TagListItem href="/">{text('Label 11', 'Long Tag  11')}</TagListItem>
        <TagListItem href="/">{text('Label 12', 'Long Tag  12')}</TagListItem>
        <TagListItem href="/">{text('Label 13', 'Very Long Tag  13')}</TagListItem>
        <TagListItem href="/">{text('Label 14', 'Long Tag  14')}</TagListItem>
        <TagListItem href="/">{text('Label 15', 'Long Tag  15')}</TagListItem>
        <TagListItem href="/">{text('Label 16', 'Long Tag  16')}</TagListItem>
        <TagListItem href="/">{text('Label 17', 'Very Long Tag  17')}</TagListItem>
      </TagList>
    </div>

    <style jsx>{`
      .container {
        font-family: 'Roboto', sans-serif;
        padding: 40px;
        background-color: #F3F5F5;
      }
  `}</style>
  </Block>
)
