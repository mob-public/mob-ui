import React from 'react'
import { select, text, withKnobs } from '@storybook/addon-knobs'
import { Paper } from '../src/components/Paper'
import { Block } from './Block'

export default {
  title: 'Paper',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <Paper
      background={text('Background', '#FFFFFF')}
      shadowBlur={text('Shadow blur', '5px')}
      shadowColor={text('Shadow color', 'rgba(0, 0, 0, 0.2)')}
      borderRadius={text('Border radius', '7px')}
      className={select('Class name', ['', 'desktop-rounded'], '')}
    >
      <div style={{ padding: 20 }}>
        {text('Content', 'Lorem ipsum dolor sit amet.')}
      </div>
    </Paper>

    <style jsx>{`
      @media(min-width: 960px) {
        :global(.desktop-rounded) {
          border-radius: 7px;
        }
      }
  `}</style>
  </Block>
)
