import React from 'react'
import { boolean, text, withKnobs } from '@storybook/addon-knobs'
import { Photostatus } from '../src/components/Photostatus'
import { Block } from './Block'

export default {
  title: 'Photostatus',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <Photostatus
      backgroundImage={text('Background image', 'https://mobimg.b-cdn.net/v2/fetch/73/73a9205c51fc77e1c55feb51da3eeca9.jpeg')}
      darken={boolean('Darken', true)}
      blur={boolean('Blur', false)}
      gradient={boolean('Gradient', false)}
    >
      <div className="content">
        {text('Content', 'Lorem ipsum dolor sit amet.')}
      </div>
    </Photostatus>

    <style jsx>{`
      .content {
        width: 800px;
        height: 400px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #FFFFFF;
      }
  `}</style>
  </Block>
)
