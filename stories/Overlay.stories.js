import React, { useState } from 'react'
import { withKnobs, boolean, number } from '@storybook/addon-knobs'
import { Overlay } from '../src/components/Overlay'
import { Button } from '../src/components/Button'
import { Block } from './Block'

export default {
  title: 'Overlay',
  decorators: [withKnobs],
}

export const Playground = () => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <Block title="Playground">
      <Button onClick={() => setIsOpen(0)} theme="black">
        Open overlay
      </Button>
      {isOpen === 0 && (
        <Overlay
          withAnimation={boolean('Animation', false)}
          marginTop={number('Margin Top', 0)}
          onCloseRequest={() => setIsOpen(false)}
        />
      )}
    </Block>
  )
}
