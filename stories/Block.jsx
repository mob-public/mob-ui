import React from 'react'
import PropTypes from 'prop-types'

export const Block = ({ title, children }) => (
  <div className="block">
    <h2 className="block__heading">{title}</h2>
    <div className="block__children">{children}</div>

    <style jsx>{`
      .block {
        margin-bottom: 100px;
      }

      .block__heading {
        background: #eeeeee;
        padding: 20px 30px;
        border-radius: 40px 10px 40px 10px;
      }

      .block__children {
        border: 1px solid rgba(0, 0, 0, 0.12);
        padding: 49px;
        margin: 0 auto;
        display: flex;
        flex-direction: column;
        align-items: center;
      }
    `}</style>
  </div>
)

Block.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}
