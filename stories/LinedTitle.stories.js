import React from 'react'
import { select, text, withKnobs } from '@storybook/addon-knobs'
import { LinedTitle } from '../src/components/LinedTitle'
import { Block } from './Block'

export default {
  title: 'LinedTitle',
  decorators: [withKnobs],
}

export const Playground = () => (
  <Block title="Playground">
    <div className="container">
      <LinedTitle
        linePosition={select('Line position', [undefined, 'left', 'right'], undefined)}
        color={text('Color', '#898989')}
        lineColor={text('Line color', '#C4C4C4')}
        margin={text('Margin', '15px')}
        fontSize={text('Font size', '12px')}
      >
        {text('Label', 'Lined Title')}
      </LinedTitle>
    </div>

    <style jsx>{`
      .container { width: 400px; }
  `}</style>
  </Block>
)
