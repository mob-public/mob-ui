import React from 'react'
import { withKnobs, text, number, boolean } from '@storybook/addon-knobs'
import { Block } from './Block'
import { ButtonCustom } from '../src/components/ButtonCustom'

export default {
  title: 'ButtonCustom',
  decorators: [withKnobs],
}

export const Playground = () => (
  <>
    <Block title="Playground">
      <ButtonCustom
        fontSize={number('fontSize', 16)}
        borderRadius={number('borderRadius', 7)}
        height={number('height', 40)}
        color={text('color', '#fff')}
        background={text('backgroud', '#24C77B')}
        minWidth={number('minWidth', 150)}
        children={text('Label', 'Label')}
        icon={text('Icon', undefined)}
        iconRight={text('Icon right', undefined)}
        disabled={boolean('Disabled', false)}
      />
    </Block>
  </>
)
