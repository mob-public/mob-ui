import React from 'react'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { Block } from './Block'
import { Button, themes, sizes, shapes, iconPosition } from '../src/components/Button'

export default {
  title: 'Button',
  decorators: [withKnobs],
}

export const Playground = () => (
  <>
    <Block title="Playground">
      <Button
        children={text('Label', 'Button Label')}
        theme={select('Theme', themes, 'green')}
        icon={text('Icon', '')}
        iconPosition={select('Icon Position', iconPosition, 'left')}
        shape={select('Shape', shapes, 'circle')}
        size={select('Size', sizes, 40)}
        block={boolean('Is Block', false)}
        short={boolean('Is Short', false)}
        alignLeft={boolean('Align left', false)}
      />
    </Block>

    <Block title="Themes">
      <div style={{ display: 'grid', gridGap: 20, gridTemplateColumns: '200px 200px 200px 200px' }}>
        <Button style={{ background: '#eeeeee' }}>Transparent</Button>
        <Button theme="black">Black</Button>
        <Button theme="green">Green</Button>
        <Button theme="gray">Gray</Button>
        <Button theme="dark-gray">Dark-gray</Button>
        <Button theme="blue">Blue</Button>
        <Button theme="white">White</Button>
        <Button theme="white-blue">White-blue</Button>
        <Button theme="white-green">White-green</Button>
        <Button theme="white-red">White-red</Button>
        <Button theme="white-gray">White-gray</Button>
        <Button theme="white-gray-2">White-gray-2</Button>
      </div>
    </Block>
  </>
)
