import React from 'react'
import PropTypes from 'prop-types'

export const Paper = (props) => {
  const { background, shadowColor, shadowBlur, borderRadius, ...restProps } = props
  const boxShadow = `0 0 ${shadowBlur} ${shadowColor}`
  return (
    <div style={{ background, boxShadow, borderRadius }} {...restProps} />
  )
}

Paper.propTypes = {
  background: PropTypes.string,
  shadowBlur: PropTypes.string,
  shadowColor: PropTypes.string,
  borderRadius: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

Paper.defaultProps = {
  background: '#FFFFFF',
  shadowBlur: '15px',
  shadowColor: 'rgba(0, 0, 0, 0.05)',
  borderRadius: undefined,
}
