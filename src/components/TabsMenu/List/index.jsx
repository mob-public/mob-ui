import React, { Children } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const TabsMenu = ({ children, className }) => (
  <>
    <ul className={`list ${className}`}>
      {Children.map(children, (child) => (
        <li className="list__item" key={child}>
          {child}
        </li>
      ))}
    </ul>

    <style jsx>{styles}</style>
  </>
)

TabsMenu.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
}

TabsMenu.defaultProps = {
  className: '',
}
