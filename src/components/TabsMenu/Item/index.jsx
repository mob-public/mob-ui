import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const TabsMenuItem = ({ isActive, children }) => (
  <div className={`item ${isActive ? 'item_active' : ''}`}>
    <div className="inner">
      {children}
    </div>
    <style jsx>{styles}</style>
  </div>
)

TabsMenuItem.propTypes = {
  children: PropTypes.node.isRequired,
  isActive: PropTypes.bool,
}

TabsMenuItem.defaultProps = {
  isActive: false,
}
