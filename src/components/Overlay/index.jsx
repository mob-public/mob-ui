import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const Overlay = ({
  onCloseRequest,
  withAnimation,
  marginTop,
  ...restProps
}) => {
  useEffect(() => {
    const topPosition = window.scrollY
    document.documentElement.classList.add('with-overlay')
    document.body.style.top = `-${topPosition}px`

    // fix iphone position: fixed
    setTimeout(() => window.scrollTo(0, 0), 0)

    return () => {
      document.documentElement.classList.remove('with-overlay')
      document.body.style.top = null
      window.scrollTo(0, topPosition)
    }
  }, [])

  return (
    <div
      className={`overlay ${withAnimation ? 'overlay_animated' : ''}`}
      onClick={onCloseRequest}
      onTouchMove={onCloseRequest}
      {...restProps}
    >
      <div className="background" style={{ marginTop }} />

      <style jsx>{styles}</style>
    </div>
  )
}

Overlay.propTypes = {
  onCloseRequest: PropTypes.func,
  withAnimation: PropTypes.bool,
  marginTop: PropTypes.number,
}

Overlay.defaultProps = {
  onCloseRequest: undefined,
  withAnimation: false,
  marginTop: 0,
}
