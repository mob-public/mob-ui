import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const Photostatus = ({ backgroundImage, children, darken, blur, gradient,
  ...imgOptions }) => {
  const classes = ['background']
  if (darken) classes.push('background_darkened')
  if (blur) classes.push('background_blurred')
  if (gradient) classes.push('background_gradient')

  return (
    <>
      <div className="photostatus">
        <div className={classes.join(' ')}>
          <img
            className="image"
            src={backgroundImage}
            alt=""
            {...imgOptions}
          />
        </div>
        <div className="children">{children}</div>
      </div>

      <style jsx>{styles}</style>
    </>
  )
}

Photostatus.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  children: PropTypes.node,
  darken: PropTypes.bool,
  blur: PropTypes.bool,
  gradient: PropTypes.bool,
}

Photostatus.defaultProps = {
  children: undefined,
  darken: true,
  blur: false,
  gradient: false,
}
