import React from 'react'
import PropTypes from 'prop-types'

export const Checkbox = ({ id, label, size, borderRadius, color, ...restProps }) => (
  <label className="checkbox" htmlFor={id}>
    {label}
    <input
      id={id}
      type="checkbox"
      className="input"
      {...restProps}
    />
    <div className="checkmark" />

    <style jsx>{`
      .input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
      }
      
      .checkbox {
        display: block;
        position: relative;
        padding-left: ${size}px;
        cursor: pointer;
      }
      
      .checkmark {
        content: '';
        width: ${size}px;
        height: ${size}px;
        position: absolute;
        left: 0;
        top: 0;
        background-color: #FFFFFF;
        border: 1px solid #E6E8EA;
        border-radius: ${borderRadius}px;
        background-image: url("data:image/svg+xml,%3Csvg width='18' height='30' viewBox='0 0 18 30' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M5.6 18.6L1.4 14.4L0 15.8L5.6 21.4L17.6 9.4L16.2 8L5.6 18.6Z' fill='white'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: center center;
      }
      
      .input:checked + .checkmark {
        background-color: ${color};
      }

  `}</style>
  </label>
)

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.node,
  size: PropTypes.number,
  borderRadius: PropTypes.number,
  color: PropTypes.string,
  icon: PropTypes.node,
}

Checkbox.defaultProps = {
  label: undefined,
  size: 10,
  borderRadius: 2,
  color: '#1BB378',
  icon: undefined,
}
