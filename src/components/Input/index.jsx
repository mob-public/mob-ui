import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

// eslint-disable-next-line
export const Input = ({
  className,
  theme,
  size,
  shape,
  leftElement,
  rightElement,
  onFocus,
  onBlur,
  ...props
}) => {
  const [focused, setFocused] = useState(false)

  const classes = [
    'input',
    `input_theme_${theme}`,
    `input_size_${size}`,
    `input_shape_${shape}`,
    className,
  ]
  if (focused) classes.push('input_focused')

  const handleFocus = e => {
    setFocused(true)
    onFocus(e)
  }

  const handleBlur = e => {
    setFocused(false)
    onBlur(e)
  }

  return (
    <>
      <div className={classes.join(' ')}>
        {leftElement && <div className="left-element">{leftElement}</div>}
        <input
          className="input__input"
          type="text"
          onFocus={handleFocus}
          onBlur={handleBlur}
          {...props}
        />
        {rightElement && <div className="right-element">{rightElement}</div>}
      </div>

      <style jsx>{styles}</style>
    </>
  )
}

export const themes = ['bright', 'dark', 'gray']
export const sizes = ['small', 'middle', 'big']
export const shapes = ['box', 'round']

Input.propTypes = {
  className: PropTypes.string,
  rightElement: PropTypes.node,
  leftElement: PropTypes.node,
  theme: PropTypes.oneOf(themes),
  size: PropTypes.oneOf(sizes),
  shape: PropTypes.oneOf(shapes),
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
}

Input.defaultProps = {
  className: '',
  rightElement: undefined,
  leftElement: undefined,
  theme: 'bright',
  size: 'middle',
  shape: 'box',
  onFocus: () => {},
  onBlur: () => {},
}
