import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'
import { lightenDarkenColor } from '../../utils/lightenDarkenColor'

export const ButtonCustom = forwardRef((props, ref) => {
  const {
    component,
    icon,
    iconRight,
    children,
    height,
    color,
    background,
    backgroundHover,
    borderRadius,
    fontSize,
    minWidth,
    ...elementProps
  } = props

  const Element = elementProps.href ? 'a' : component
  const classes = ['button']
  const onlyIcon = icon && !children
  if (onlyIcon) classes.push('button_type_icon')

  return (
    <Element className={classes.join(' ')} ref={ref} {...elementProps}>
      {icon && (
        <div className={`icon ${children ? 'icon_left' : ''}`}>{icon}</div>
      )}

      {children && <div className="children">{children}</div>}

      {iconRight && <div className="icon  icon_right">{iconRight}</div>}

      <style jsx>{styles}</style>
      <style jsx>
        {`
          .button {
            height: ${height}px;
            color: ${color};
            background: ${background};
            border-radius: ${borderRadius}px;
            font-size: ${fontSize}px;
            min-width: ${onlyIcon ? height : minWidth}px;
          }

          .button:hover {
            background: ${backgroundHover
            || lightenDarkenColor(background, -20)} !important;
          }
        `}
      </style>
    </Element>
  )
})

ButtonCustom.propTypes = {
  background: PropTypes.string,
  backgroundHover: PropTypes.string,
  color: PropTypes.string,
  height: PropTypes.number,
  borderRadius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  fontSize: PropTypes.number,
  minWidth: PropTypes.number,
  component: PropTypes.node,
  href: PropTypes.string,
  icon: PropTypes.node,
  iconRight: PropTypes.node,
  children: PropTypes.node,
}

ButtonCustom.defaultProps = {
  background: '#fff',
  backgroundHover: undefined,
  color: '#111',
  height: 40,
  borderRadius: 0,
  fontSize: 16,
  minWidth: 150,
  component: 'button',
  href: undefined,
  icon: undefined,
  iconRight: undefined,
  children: undefined,
}
