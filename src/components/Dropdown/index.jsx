import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import useEscapeKey from '../../hooks/useEscapeKey'
import styles from './index.css'

export const Dropdown = (props) => {
  const { content, children, shape, withPointer, shadowOpacity } = props
  const [isOpen, setIsOpen] = useState(false)
  const [position, setPosition] = useState(props.position)
  const containerRef = useRef(null)
  const contentRef = useRef(null)

  useEscapeKey(() => setIsOpen(false), [setIsOpen])

  useEffect(() => {
    setPosition(props.position)
  }, [setPosition, props.position])

  useEffect(() => {
    if (!isOpen || position !== 'center') {
      return
    }
    const { left, right, width } = contentRef.current.getBoundingClientRect()
    if (width > window.innerWidth) {
      return
    }
    if (right > window.innerWidth) {
      setPosition('left')
    }
    if (left < 0) {
      setPosition('right')
    }
  }, [isOpen, setPosition, position])

  useEffect(() => {
    if (!isOpen) {
      return undefined
    }

    const handleClickOutside = (e) => {
      if (!containerRef.current.contains(e.target)) {
        setIsOpen(false)
      }
    }

    document.addEventListener('mousedown', handleClickOutside)

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [isOpen])

  const borderRadius = shape === 'round' ? '7px' : undefined
  const classes = [
    'dropdown',
    `position_${position}`,
    isOpen ? 'open' : '',
    withPointer ? 'with-pointer' : '',
  ]

  return (
    <>
      <div className={classes.join(' ')} ref={containerRef}>
        <div className="trigger" onClick={() => setIsOpen(!isOpen)}>
          {children(isOpen)}
        </div>
        {isOpen && (
          <div className="content" ref={contentRef}>
            <div className="inner" style={{ borderRadius }}>
              {typeof content === 'function'
                ? content(() => setIsOpen(false))
                : content}
            </div>
          </div>
        )}
      </div>

      <style jsx>{styles}</style>
      <style jsx>{`
        .inner {
          box-shadow: 0 0 30px rgba(0, 0, 0, ${shadowOpacity});
        }
    `}</style>
    </>
  )
}

export const positions = ['left', 'center', 'right']
export const shapes = ['square', 'round']

Dropdown.propTypes = {
  children: PropTypes.func.isRequired,
  content: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  position: PropTypes.oneOf(positions),
  shape: PropTypes.oneOf(shapes),
  withPointer: PropTypes.bool,
  shadowOpacity: PropTypes.number,
}

Dropdown.defaultProps = {
  content: undefined,
  position: 'center',
  shape: 'square',
  withPointer: false,
  shadowOpacity: 0.05,
}
