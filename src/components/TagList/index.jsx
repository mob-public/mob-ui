import React, { Children, useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const TagList = ({ children, showAllButton, rightButton }) => {
  const innerRef = useRef(null)
  const [isExtendable, setIsExtendable] = useState(false)
  const [isExtended, setIsExtended] = useState(false)
  const classes = ['tag-list']
  if (isExtended) {
    classes.push('tag-list_extended')
  }

  const handleClickShowAll = () => {
    setIsExtended(true)
  }

  useEffect(() => {
    if (!showAllButton) {
      return
    }
    const isWrapped = [...innerRef.current.children].some((item, i, items) => {
      const prevItem = items[i - 1]
      return prevItem && item.offsetTop > prevItem.offsetTop
    })
    if (isWrapped) {
      setIsExtendable(true)
    }
  }, [showAllButton])

  return (
    <div className={classes.join(' ')}>
      <ul className="inner" ref={innerRef}>
        {Children.map(children, (child) => (
          <li key={child} className="item">
            {child}
            &nbsp;
          </li>
        ))}
        {rightButton && isExtended && (
          <li className="item">{rightButton}</li>
        )}
      </ul>
      {showAllButton && isExtendable && !isExtended && (
        <div className="show-all-button" onClick={handleClickShowAll}>
          {showAllButton}
        </div>
      )}
      {rightButton && !isExtended && (
        <div className="right-button">{rightButton}</div>
      )}
      <style jsx>{styles}</style>
    </div>
  )
}

TagList.propTypes = {
  children: PropTypes.node,
  showAllButton: PropTypes.node,
  rightButton: PropTypes.node,
}

TagList.defaultProps = {
  children: [],
  showAllButton: undefined,
  rightButton: undefined,
}
