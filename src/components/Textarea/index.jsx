import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const Textarea = ({ className, ...restProps }) => (
  <>
    <textarea className={`textarea ${className}`} {...restProps} />

    <style jsx>{styles}</style>
  </>
)

Textarea.propTypes = {
  className: PropTypes.string,
}

Textarea.defaultProps = {
  className: '',
}
