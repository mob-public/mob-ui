import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const Loader = ({ height, color, size, borderWidth, duration }) => (
  <div className="loader" style={{ height }}>
    <div
      className="spinner"
      style={{
        borderWidth,
        borderColor: `${color} transparent ${color} transparent`,
        width: size,
        height: size,
        animationDuration: duration,
      }}
    />

    <style jsx>{styles}</style>
  </div>
)

Loader.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  height: PropTypes.number,
  borderWidth: PropTypes.number,
  duration: PropTypes.string,
}

Loader.defaultProps = {
  color: '#0D7ED0',
  size: 30,
  height: 400,
  borderWidth: 5,
  duration: '1.2s',
}
