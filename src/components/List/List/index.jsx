import React, { Children } from 'react'
import PropTypes from 'prop-types'

export const List = ({ children, bordered, ...props }) => (
  <ul className={`list ${bordered ? 'list_bordered' : ''}`} {...props}>
    {Children.map(children, (child) => {
      if (!child) return null

      return (
        <li key={child} className="item">
          {child}
        </li>
      )
    })}

    <style jsx>
      {`
        .list {
          list-style: none;
          padding: 0;
          margin: 0;
          user-select: none;
        }

        .list_bordered > .item {
          border-bottom: 1px solid #e6e8ea;
        }

        .list_bordered > .item:last-child {
          border-bottom: 0;
        }
      `}
    </style>
  </ul>
)

List.propTypes = {
  children: PropTypes.node,
  bordered: PropTypes.bool,
}

List.defaultProps = {
  children: [],
  bordered: false,
}
