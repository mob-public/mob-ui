import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const ListItem = forwardRef(({ children, icon, iconRight, component, className,
  ...elementProps }, ref) => {
  const Element = elementProps.href ? 'a' : component
  return (
    <>
      <Element className={`list-item ${className}`} ref={ref} {...elementProps}>
        {icon && (
          <div className="icon icon_left">
            {icon}
          </div>
        )}
        <div className="children">
          {children}
        </div>
        {iconRight && (
          <div className="icon icon_right">
            {iconRight}
          </div>
        )}
      </Element>

      <style jsx>{styles}</style>
    </>
  )
})

ListItem.propTypes = {
  component: PropTypes.node,
  children: PropTypes.node.isRequired,
  icon: PropTypes.node,
  iconRight: PropTypes.node,
  href: PropTypes.string,
  className: PropTypes.string,
}

ListItem.defaultProps = {
  component: 'div',
  icon: undefined,
  iconRight: undefined,
  href: undefined,
  className: '',
}
