import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { ListItem } from '../Item'

export const ListItemExtended = ({ item, children, iconClose, iconOpen, className,
  ...itemProps }) => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <>
      <ListItem
        onClick={() => setIsOpen((value) => !value)}
        iconRight={<div style={{ pointerEvents: 'none' }}>{isOpen ? iconClose : iconOpen}</div>}
        className={`${className} list-item-extended ${isOpen ? 'list-item-extended_open' : ''}`}
        {...itemProps}
      >
        {item}
      </ListItem>

      {isOpen && children}
      <style jsx>{`
        :global(.list-item-extended) {
          cursor: pointer;
        }
        :global(.list-item-extended_open) {
          background: #F2F3F4;
        }
        :global(.list-item-extended) + :global(.list) {
          background: #F2F3F4;
        }
        :global(.list-item-extended) + :global(.list_bordered) {
          border-top: 1px solid #E6E8EA;
        }
    `}</style>
    </>
  )
}


ListItemExtended.propTypes = {
  item: PropTypes.node.isRequired,
  children: PropTypes.node,
  iconClose: PropTypes.node,
  iconOpen: PropTypes.node,
  className: PropTypes.string,
}

ListItemExtended.defaultProps = {
  children: undefined,
  iconClose: undefined,
  iconOpen: undefined,
  className: '',
}
