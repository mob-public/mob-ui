import React from 'react'
import PropTypes from 'prop-types'
import { Overlay } from '../Overlay'
import useEscapeKey from '../../hooks/useEscapeKey'
import styles from './index.css'

export const Popup = ({ children, onCloseRequest, margin }) => {
  useEscapeKey(onCloseRequest, [onCloseRequest])

  return (
    <div className="popup">
      <Overlay />
      <div className="content" onClick={onCloseRequest}>
        <div className="inner">
          <div onClick={(e) => e.stopPropagation()} style={{ margin }}>
            {children}
          </div>
        </div>
      </div>

      <style jsx>{styles}</style>
    </div>
  )
}

Popup.propTypes = {
  children: PropTypes.node.isRequired,
  onCloseRequest: PropTypes.func,
  margin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
}

Popup.defaultProps = {
  onCloseRequest: undefined,
  margin: 0,
}
