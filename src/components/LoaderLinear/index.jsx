import React from 'react'
import PropTypes from 'prop-types'

export const LoaderLinear = ({ height, backgroundColor, primaryColor, secondaryColor,
  duration }) => (
    <div className="loader-linear" style={{ height, backgroundColor }}>
      <div className="indeterminate" />

      <style jsx>{`
        .loader-linear {
          overflow: hidden;
          width: 100%;
        }
        
        .indeterminate {
          position: relative;
          width: 100%;
          height: 100%;
        }
        
        .indeterminate:before {
          content: '';
          position: absolute;
          height: 100%;
          background-color: ${primaryColor};
          animation-name: indeterminate_first;
          animation-timing-function: ease-out;
          animation-iteration-count: infinite;
          animation-duration: ${duration};
        }
        
        .indeterminate:after {
          content: '';
          position: absolute;
          height: 100%;
          background-color: ${secondaryColor};
          animation-name: indeterminate_second;
          animation-timing-function: ease-in;
          animation-iteration-count: infinite;
          animation-duration: ${duration};
        }
        
        @keyframes indeterminate_first {
          0% {
            left: -100%;
            width: 100%;
          }
          100% {
            left: 100%;
            width: 10%;
          }
        }
        
        @keyframes indeterminate_second {
          0% {
            left: -150%;
            width: 100%;
          }
          100% {
            left: 100%;
            width: 10%;
          }
        }
  
    `}</style>
    </div>
)

LoaderLinear.propTypes = {
  height: PropTypes.number,
  backgroundColor: PropTypes.string,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
  duration: PropTypes.string,
}

LoaderLinear.defaultProps = {
  height: 4,
  backgroundColor: '#B3E5FC',
  primaryColor: '#03A9F4',
  secondaryColor: '#4FC3F7',
  duration: '1.5s',
}
