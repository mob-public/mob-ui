import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styles from './index.css'

export const Button = forwardRef(({ component, theme, size, shape, block, short, className, icon,
  iconPosition, children, alignLeft, ...elementProps }, ref) => {
  const Element = elementProps.href ? 'a' : component
  const classes = [
    'button',
    `button_theme_${theme}`,
    `button_size_${size}`,
    `button_shape_${shape}`,
  ]
  if (className) classes.push(className)
  if (block) classes.push('button_block')
  if (short) classes.push('button_short')
  if (alignLeft) classes.push('button_aligned-left')
  if (icon && !children) classes.push('button_icon')

  return (
    <>
      <Element
        className={classes.join(' ')}
        ref={ref}
        {...elementProps}
      >
        <>
          {icon && iconPosition === 'left' && (
            <div className={`icon ${children ? 'icon_left' : ''}`}>
              {icon}
            </div>
          )}
          {children && (
            <div className="children">
              {children}
            </div>
          )}
          {icon && iconPosition === 'right' && (
            <div className={`icon ${children ? 'icon_right' : ''}`}>
              {icon}
            </div>
          )}
        </>
      </Element>

      <style jsx>{styles}</style>
    </>
  )
})

export const themes = ['white', 'transparent', 'green', 'gray', 'dark-gray', 'blue', 'black', 'white-gray', 'white-gray-2', 'white-green', 'white-red', 'white-blue']
export const sizes = [30, 35, 40, 45, 50, 60, 65]
export const shapes = ['box', 'round', 'circle']
export const iconPosition = ['left', 'right']

Button.propTypes = {
  component: PropTypes.node,
  href: PropTypes.string,
  theme: PropTypes.oneOf(themes),
  size: PropTypes.oneOf(sizes),
  shape: PropTypes.oneOf(shapes),
  block: PropTypes.bool,
  short: PropTypes.bool,
  className: PropTypes.string,
  icon: PropTypes.node,
  iconPosition: PropTypes.oneOf(iconPosition),
  children: PropTypes.node,
  alignLeft: PropTypes.bool,
}

Button.defaultProps = {
  component: 'button',
  href: undefined,
  theme: 'transparent',
  size: 50,
  shape: 'round',
  block: false,
  short: false,
  className: undefined,
  icon: undefined,
  iconPosition: 'left',
  children: undefined,
  alignLeft: false,
}
