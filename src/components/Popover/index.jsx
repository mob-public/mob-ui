import React from 'react'
import PropTypes from 'prop-types'
import { Overlay } from '../Overlay'
import styles from './index.css'

export const Popover = ({ children, onCloseRequest, marginTop }) => (
  <div className="popover">
    <Overlay />
    <div className="content">
      <div className="inner" onClick={onCloseRequest}>
        <div onClick={(e) => e.stopPropagation()} style={{ marginTop }}>
          {children}
        </div>
      </div>
    </div>

    <style jsx>{styles}</style>
  </div>
)

Popover.propTypes = {
  children: PropTypes.node.isRequired,
  onCloseRequest: PropTypes.func,
  marginTop: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
}

Popover.defaultProps = {
  onCloseRequest: undefined,
  marginTop: 150,
}
