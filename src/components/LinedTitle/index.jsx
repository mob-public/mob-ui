import React from 'react'
import PropTypes from 'prop-types'

export const LinedTitle = ({ linePosition, lineColor, margin, ...restProps }) => {
  const className = linePosition ? `line_${linePosition}` : 'line_left line_right'
  return (
    <>
      <div className={`title ${className}`} {...restProps} />

      <style jsx>{`
        .title.line_left:before {
          content: "";
          background-color: ${lineColor};
          height: 1px;
          flex-grow: 1;
          margin-right: ${margin};
        }
        
        .title {
          display: flex;
          align-items: center;
          word-break: break-word;
        }
        
        .title.line_right:after {
          content: "";
          background-color: ${lineColor};
          height: 1px;
          flex-grow: 1;
          margin-left: ${margin};
        }
    `}</style>
    </>
  )
}

LinedTitle.propTypes = {
  linePosition: PropTypes.oneOf(['left', 'right']),
  lineColor: PropTypes.string,
  margin: PropTypes.string,
}

LinedTitle.defaultProps = {
  linePosition: undefined,
  lineColor: '#C4C4C4',
  margin: '15px',
}
