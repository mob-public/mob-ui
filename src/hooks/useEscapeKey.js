import { useEffect } from 'react'

const ESCAPE_KEY = 27

export default (callback, deps) => {
  useEffect(() => {
    const handleKeyDown = (e) => {
      if (e.keyCode === ESCAPE_KEY) {
        callback()
      }
    }

    window.addEventListener('keydown', handleKeyDown)
    return () => {
      window.removeEventListener('keydown', handleKeyDown)
    }
  }, deps) // eslint-disable-line react-hooks/exhaustive-deps
}
