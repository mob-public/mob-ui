module.exports = {
  stories: ['../**/*.stories.js'],
  addons: [
    '@storybook/addon-storysource',
    '@storybook/addon-knobs'
  ]
};
