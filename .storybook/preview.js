import { configure, addParameters } from '@storybook/react';

addParameters({
  options: {
    name: 'Mob UI',
    panelPosition: 'bottom',
    showNav: true,
    showPanel: true,
    sidebarAnimations: true,
  }
})

// automatically import all files ending in *.stories.js
configure(require.context('../stories', true, /\.stories\.js$/), module);
