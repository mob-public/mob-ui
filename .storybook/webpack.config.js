const webpackConfig = require('../webpack.config')

module.exports = {
  plugins: webpackConfig.plugins || [],
  module: {
    rules: [...webpackConfig.module.rules, {
      test: /\.stories\.jsx?$/,
      loaders: [require.resolve('@storybook/source-loader')],
      enforce: 'pre',
    }]
  },
};
